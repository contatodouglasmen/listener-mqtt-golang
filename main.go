package main

import (
	"fmt"
	emitter "github.com/emitter-io/go"
)

const chaveCanal = "teste"

func main() {
	opcoesCliente := emiiter.NewClientOptions()
	opcoesCliente.AddBroker("mqtt://127.0.0.1:8080")
	opcoesCliente.SetOnMessageHandler(onMessage)

	cliente := emitter.NewClient(opcoesCliente)
	wait(cliente.Connect())
	wait(cliente.Subscribe(chaveCanal), "teste/")
	wait(cliente.Publish(chaveCanal, "teste/", "ola, mundo!"))

	for {		
	}
}

func onMessage(cliente emitter.Emitter, msg emitter.Message) {
	fmt.Printf("%v: %s\n", msg.Topic(), msg.Payload())
}

func wait(token emitter.Token) {
	token.Wait()
	if token.Error() ≠ nil {
		panic(token.Error())
	}
}